package fr.ign.artiscales.tools;

public class ASTools {
    private static boolean talkative = true;
    private static boolean log = false;

    public static void setTalkative(boolean talkative) {
        ASTools.talkative = talkative;
    }

    public static void setLog(boolean talkative) {
        ASTools.log = talkative;
    }

    public static void print(Object message) {
        if (talkative)
            System.out.println(message);
    }

    public static void log(Object message) {
        if (log)
            System.out.println(message);
    }

}
