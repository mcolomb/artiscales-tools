package fr.ign.artiscales.tools.util;

import java.text.Normalizer;

public class StringUtil {

    /**
     * Transform every accent or other frenchy character to regular letters.
     * From <a href="https://stackoverflow.com/questions/3322152/is-there-a-way-to-get-rid-of-accents-and-convert-a-whole-string-to-regular-lette">...</a>
     *
     * @param string input string to transform
     * @return The same flat string
     */
    public static String flattenToAscii(String string) {
        if (string == null)
            return null;
        char[] out = new char[string.length()];
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        int j = 0;
        for (int i = 0, n = string.length(); i < n; ++i) {
            char c = string.charAt(i);
            if (c <= '\u007F') out[j++] = c;
        }
        return new String(out);
    }
}
