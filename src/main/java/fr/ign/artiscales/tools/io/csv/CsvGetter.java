package fr.ign.artiscales.tools.io.csv;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import fr.ign.artiscales.tools.geoToolsFunctions.Attribute;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class CsvGetter extends Csv {


    /**
     * Get the number of occurrences for each unique values for a given field from a .csv file
     *
     * @param csvFile   csv file
     * @param fieldName name of the field to sum values
     * @return a map of unique values with how many times they're used
     */
    public static HashMap<String, Integer> countFieldValue(File csvFile, String fieldName) throws IOException {
        CSVReader r = getCSVReader(csvFile);
        int i = Attribute.getIndice(r, fieldName);
        HashMap<String, Integer> result = new HashMap<>();
        Iterator<String[]> it = r.iterator();
        while (it.hasNext()) {
            String[] l = it.next();
            if (!result.containsKey(l[i]))
                result.put(l[i], 1);
            else
                result.put(l[i], result.get(l[i]) + 1);
        }
        r.close();
        return result;
    }


    /**
     * Get the values of cells corresponding to the value of another field's cell.
     *
     * @param csvFile              CSV {@link File} with a comma as a separator
     * @param targetAttributeName  name of the field that will be compared
     * @param targetAttributeValue value of the cell that will be compared
     * @param wantedAttributeName  name of the field of the wanted cell
     * @return the value of the cells in an {@link ArrayList}
     * @throws IOException by CSVReader
     */
    public static List<String> getCells(File csvFile, String targetAttributeName, String targetAttributeValue, String wantedAttributeName) throws IOException {
        return getCells(csvFile, new String[]{targetAttributeName}, new String[]{targetAttributeValue}, wantedAttributeName);
    }

    /**
     * Return true if the combination of field's values are found in a CSV reader.
     *
     * @param r                     CSV reader with a header
     * @param targetAttributeNames  names of the field that will be compared.
     * @param targetAttributeValues values of the field that will be compared. Must be in the same order (and the same length) than the #targetAttributeNames.
     * @return the values of the cell
     * @throws IOException by CSVReader
     */
    public static boolean isCellsContainCorrespondingCombination(CSVReader r, String[] targetAttributeNames, String[] targetAttributeValues) throws IOException {
        String[] fLine;
        try {
            fLine = r.readNext();
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }
        Iterator<String[]> it = r.iterator();
        while (it.hasNext()) {
            String[] line = it.next();
            boolean add = true;
            for (int i = 0; i < targetAttributeNames.length; i++)
                if (!line[Attribute.getIndice(fLine, targetAttributeNames[i])].equals(targetAttributeValues[i])) {
                    add = false;
                    break;
                }
            if (add)
                return true;
        }
        return false;
    }

    public static boolean isCellsContainCorrespondingCombination(File f, String[] targetAttributeNames, String[] targetAttributeValues) throws IOException {
        CSVReader r = Csv.getCSVReader(f);
        boolean answer = isCellsContainCorrespondingCombination(r, targetAttributeNames, targetAttributeValues);
        r.close();
        return answer;
    }

    /**
     * Get the value of a cell corresponding to the value of multiple field's cells.
     *
     * @param csvFile               CSV {@link File}
     * @param targetAttributeNames  name of the field that will be compared
     * @param targetAttributeValues values of the cells that will be compared
     * @param wantedAttributeName   name of the field of the wanted cell
     * @return the values of the cell
     * @throws IOException by CSVReader
     */
    public static List<String> getCells(File csvFile, String[] targetAttributeNames, String[] targetAttributeValues, String wantedAttributeName) throws IOException {
        CSVReader r = getCSVReader(csvFile);
        String[] fLine;
        try {
            fLine = r.readNext();
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }
        List<String> result = new ArrayList<>();
        Iterator<String[]> it = r.iterator();
        int indice = Attribute.getIndice(fLine, wantedAttributeName);
        while (it.hasNext()) {
            String[] line = it.next();
            boolean add = true;
            for (int i = 0; i < targetAttributeNames.length; i++)
                if (!line[Attribute.getIndice(fLine, targetAttributeNames[i])].equals(targetAttributeValues[i])) {
                    add = false;
                    break;
                }
            if (add)
                result.add(line[indice]);
        }
        r.close();
        return result;
    }

    /**
     * Get the values of cells corresponding to the value of multiple field's cells.
     *
     * @param csvFile               CSV {@link File}
     * @param targetAttributeNames  name of the field that will be compared
     * @param targetAttributeValues values of the cells that will be compared
     * @param wantedAttributeName   name of the field of the wanted cell
     * @return the values of the cell
     * @throws IOException by CSVReader
     */
    public static List<String[]> getCells(File csvFile, String[] targetAttributeNames, String[] targetAttributeValues, String[] wantedAttributeName) throws IOException {
        CSVReader r = getCSVReader(csvFile);
        String[] fLine;
        try {
            fLine = r.readNext();
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }
        List<String[]> result = new ArrayList<>();
        Iterator<String[]> it = r.iterator();
        List<Integer> indice = new ArrayList<>(wantedAttributeName.length);
        for (String att : wantedAttributeName)
            indice.add(Attribute.getIndice(fLine, att));

        while (it.hasNext()) {
            String[] line = it.next();
            boolean add = true;
            for (int i = 0; i < targetAttributeNames.length; i++)
                if (!line[Attribute.getIndice(fLine, targetAttributeNames[i])].equals(targetAttributeValues[i])) {
                    add = false;
                    break;
                }
            if (add)
                result.add(indice.stream().map(i -> line[i]).toArray(String[]::new));
        }
        r.close();
        return result;
    }

    /**
     * Get the value of a cell corresponding to the value of another field's cell. Unique result, stop at first.
     *
     * @param csvFile              CSV {@link File} with a comma as a separator
     * @param targetAttributeName  name of the field that will be compared
     * @param targetAttributeValue value of the cell that will be compared
     * @param wantedAttributeName  name of the field of the wanted cell
     * @return the values of the cell
     * @throws IOException by CSVReader
     */
    public static String getCell(File csvFile, String targetAttributeName, String targetAttributeValue, String wantedAttributeName) throws IOException {
        CSVReader r = getCSVReader(csvFile);
        String s = getCell(r, targetAttributeName, targetAttributeValue, wantedAttributeName);
        r.close();
        return s;
    }

    /**
     * Get the value of a cell corresponding to the value of another field's cell. Unique result, stop at first.
     *
     * @param csvInputStream       CSV {@link InputStream} with a comma as a separator
     * @param targetAttributeName  name of the field that will be compared
     * @param targetAttributeValue value of the cell that will be compared
     * @param wantedAttributeName  name of the field of the wanted cell
     * @return the values of the cell
     * @throws IOException by CSVReader
     */
    public static String getCell(InputStream csvInputStream, String targetAttributeName, String targetAttributeValue, String wantedAttributeName) throws IOException {
        CSVReader r = getCSVReader(csvInputStream);
        assert r != null;
        String s = getCell(r, targetAttributeName, targetAttributeValue, wantedAttributeName);
        r.close();
        return s;
    }

    /**
     * Get the value of a cell corresponding to the value of another field's cell. Unique result, stop at first.
     *
     * @param r                    Reader which won't be closed
     * @param targetAttributeName  name of the field that will be compared
     * @param targetAttributeValue value of the cell that will be compared
     * @param wantedAttributeName  name of the field of the wanted cell
     * @return the values of the cell
     * @throws IOException by CSVReader
     */
    public static String getCell(CSVReader r, String targetAttributeName, String targetAttributeValue, String wantedAttributeName) throws IOException {
        assert r != null;
        String[] fLine;
        try {
            fLine = r.readNext();
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }
        return getCell(r.iterator(), fLine, targetAttributeName, targetAttributeValue, wantedAttributeName);
    }

    public static String getCell(Iterator<String[]> it, String[] fLine, String targetAttributeName, String targetAttributeValue, String wantedAttributeName) throws FileNotFoundException {
        String result = "";
        int iTarget = Attribute.getIndice(fLine, targetAttributeName);
        while (it.hasNext()) {
            String[] line = it.next();
            if (line[iTarget].equals(targetAttributeValue)) {
                result = line[Attribute.getIndice(fLine, wantedAttributeName)];
                break;
            }
        }
        return result;
    }

    /**
     * Get the value of a cell corresponding to the value of another field's cell. Unique result, stop at first.
     *
     * @param csvFile               CSV {@link File} with a comma as a separator
     * @param targetAttributeNames  names of the fields that will be compared
     * @param targetAttributeValues values of the cells that will be compared
     * @return the values of the cell
     * @throws IOException by CSVReader
     */
    public static String[] getLine(File csvFile, String[] targetAttributeNames, String[] targetAttributeValues) throws IOException {
        CSVReader r = getCSVReader(csvFile);
        String[] fLine;
        try {
            fLine = r.readNext();
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }
        String[] result = new String[fLine.length];
        Iterator<String[]> it = r.iterator();
        while (it.hasNext()) {
            String[] line = it.next();
            boolean add = true;
            for (int i = 0; i < targetAttributeNames.length; i++)
                if (!line[Attribute.getIndice(fLine, targetAttributeNames[i])].equals(targetAttributeValues[i])) {
                    add = false;
                    break;
                }
            if (add) {
                result = line;
                break;
            }
        }
        r.close();
        return result;
    }

    /**
     * Get the indice number on the position of the header of a .csv file
     *
     * @param csvFile   .csv file with a header
     * @param fieldName name of the field to get indice from
     * @return the indice on which number
     */
    public static int getIndice(File csvFile, String fieldName) throws IOException {
        CSVReader r = getCSVReader(csvFile);
        int i = Attribute.getIndice(r, fieldName);
        r.close();
        return i;
    }

    /**
     * Get each distinct values of a field from a .csv
     *
     * @param csvFile input file
     * @param field   name of the concerned field
     * @return a list with every distinct values
     * @throws IOException reading .csv
     */
    public static List<String> getDistinctValue(File csvFile, String field) throws IOException {
        ArrayList<String> distinct = new ArrayList<>();
        CSVReader r = getCSVReader(csvFile);
        int i = Attribute.getIndice(r, field);
        Iterator<String[]> it = r.iterator();
        while (it.hasNext()) {
            String[] s = it.next();
            if (!distinct.contains(s[i]))
                distinct.add(s[i]);
        }
        r.close();
        return distinct;
    }

    /**
     * Count the number of occurrences of distinct values of a field from a .csv
     *
     * @param csvFile input file
     * @param field   name of the concerned field
     * @return a collection with a distinct value and each time it is used
     * @throws IOException reading .csv
     */
    public static HashMap<String, Long> countDistinctValue(File csvFile, String field) throws IOException {
        HashMap<String, Long> distinct = new HashMap<>();
        CSVReader r = getCSVReader(csvFile);
        int i = Attribute.getIndice(r, field);
        Iterator<String[]> it = r.iterator();
        while (it.hasNext()) {
            String[] s = it.next();
            if (!distinct.containsKey(s[i]))
                distinct.put(s[i], 1L);
            else
                distinct.replace(s[i], distinct.get(s[i]) + 1);
        }
        r.close();
        return distinct;
    }
}