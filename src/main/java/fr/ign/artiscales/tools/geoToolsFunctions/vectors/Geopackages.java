package fr.ign.artiscales.tools.geoToolsFunctions.vectors;

import fr.ign.artiscales.tools.ASTools;
import fr.ign.artiscales.tools.geoToolsFunctions.vectors.collec.CollecMgmt;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.geopkg.GeoPkgDataStoreFactory;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Geopackages {

//    public static void main(String[] args) throws IOException {
//        mergeGpkgFiles((Arrays.stream(new File("/home/mcolomb/INRIA/popSynth/Paris/").listFiles()).filter(f -> f.getName().startsWith("Individual")).toList()), new File("/tmp/merged.gpkg"), null, true, true);
//    }

    public static DataStore getDataStore(URL url) throws IOException {
        HashMap<String, Object> map = new HashMap<>();
        map.put(GeoPkgDataStoreFactory.DBTYPE.key, "geopkg");
        map.put(GeoPkgDataStoreFactory.DATABASE.key, url.toExternalForm());
        return DataStoreFinder.getDataStore(map);
    }

    public static DataStore getDataStore(File file) throws IOException {
        if (!file.exists() || !file.getName().endsWith(".gpkg")) {
            ASTools.print(file + " doesn't exists or is not a geopackage");
            return null;
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put(GeoPkgDataStoreFactory.DBTYPE.key, "geopkg");
        map.put(GeoPkgDataStoreFactory.DATABASE.key, file.getPath());
        return DataStoreFinder.getDataStore(map);
    }

    public static File exportSFCtoGPKG(SimpleFeatureCollection toExport, File fileOut, SimpleFeatureType ft, boolean overwrite) throws IOException {
        if (!fileOut.getName().endsWith(".gpkg"))
            fileOut = new File(fileOut + ".gpkg");
        if (fileOut.exists())
            if (overwrite)
                Files.delete(fileOut.toPath());
            else
                return mergeGpkg(toExport, fileOut);
        Map<String, Object> params = new HashMap<>();
        params.put(GeoPkgDataStoreFactory.DBTYPE.key, "geopkg");
        params.put(GeoPkgDataStoreFactory.DATABASE.key, fileOut.getPath());
        params.put("create spatial index", Boolean.TRUE);
        DataStore newDataStore = DataStoreFinder.getDataStore(params);
        newDataStore.createSchema(ft);
        File result = CollecMgmt.makeTransaction(newDataStore, toExport, fileOut, ft);
        newDataStore.dispose();
        return result;
    }

    /**
     * Merge multiple {@link SimpleFeatureCollection} into different layers of a geopackage. Every SFC names must be different.
     *
     * @param sfcs    array of sfc
     * @param outFile single saved file
     * @return the single saved file
     * @throws IOException by deleting existing outFile
     */
    public static File mergeGpkgOnDifferentLayers(SimpleFeatureCollection[] sfcs, File outFile) throws IOException {
        // check if typenames are right
        List<String> lNames = new ArrayList<>();
        for (SimpleFeatureCollection sfc : sfcs) {
            String typeName = sfc.getSchema().getTypeName();
            if (lNames.contains(typeName)) {
                ASTools.print("mergeGpkgOnDifferentLayers: collection already have the " + typeName + " layer. Returning null");
                return null;
            } else
                lNames.add(typeName);
        }
        if (!outFile.getName().endsWith(".gpkg"))
            outFile = new File(outFile + ".gpkg");
        if (outFile.exists())
            Files.delete(outFile.toPath());
        GeoPkgDataStoreFactory factory = new GeoPkgDataStoreFactory();
        Map<String, Object> params = new HashMap<>();
        params.put("url", outFile.toPath());
        params.put(GeoPkgDataStoreFactory.DBTYPE.key, "geopkg");
        params.put(GeoPkgDataStoreFactory.DATABASE.key, outFile.getPath());
        params.put("create spatial index", Boolean.TRUE);
        DataStore dataStore = factory.createDataStore(params);

        for (SimpleFeatureCollection sfc : sfcs)
            dataStore.createSchema(sfc.getSchema());
        // make transaction
        for (int i = 0; i < sfcs.length; i++) {
            final int j = i;
            String typeName = Arrays.stream(dataStore.getTypeNames()).filter(name -> name.equals(sfcs[j].getSchema().getTypeName())).findFirst().orElse("");
            CollecMgmt.makeTransaction(dataStore, sfcs[i], null, dataStore.getFeatureSource(typeName).getSchema(), typeName);
        }
        dataStore.dispose();
        return outFile;
    }

    public static File mergeGpkg(SimpleFeatureCollection toAdd, File existingGpkg) throws IOException {
        File tmpFile = new File(existingGpkg.getParentFile(), existingGpkg.getName().replace(".gpkg", "-temp.gpkg"));
        Files.copy(existingGpkg.toPath(), new FileOutputStream(tmpFile));
        Files.delete(existingGpkg.toPath());
        DataStore ds = getDataStore(tmpFile);
        assert ds != null;
        CollecMgmt.exportSFC(CollecMgmt.mergeSFC(Arrays.asList(toAdd, ds.getFeatureSource(ds.getTypeNames()[0]).getFeatures()),
                true, null), existingGpkg);
        ds.dispose();
        Files.delete(tmpFile.toPath());
        return existingGpkg;
    }

    public static File mergeGpkgFiles(List<File> file2MergeIn, File f) throws IOException {
        return mergeGpkgFiles(file2MergeIn, f, null, true);
    }

    public static File mergeGpkgFiles(List<File> filesToMerge, File fileOut, File boundFile, boolean keepAttributes) throws IOException {
        return mergeGpkgFiles(filesToMerge, fileOut, boundFile, keepAttributes, false);
    }

    public static File mergeGpkgFiles(List<File> filesToMerge, File fileOut, File boundFile, boolean keepAttributes, boolean largeFiles) throws IOException {
        // stupid basic checkout
        if (filesToMerge.isEmpty()) {
            ASTools.print("mergeGpkgFiles: list empty, " + fileOut + " null");
            return null;
        }
        // verify that every file exists and start a new function with clean list if not
        if (filesToMerge.stream().anyMatch(f -> !f.exists())) {
            filesToMerge.stream().filter(f -> !f.exists()).forEach(file -> ASTools.print(file + " doesn't exists"));
            return mergeGpkgFiles(filesToMerge.stream().filter(File::exists).collect(Collectors.toList()), fileOut, boundFile, keepAttributes);
        }
        if (!largeFiles) {
            List<SimpleFeatureCollection> sfcs = new ArrayList<>(filesToMerge.size());
            for (File f : filesToMerge) {
                DataStore ds = getDataStore(f);
                assert ds != null;
                sfcs.add(DataUtilities.collection(ds.getFeatureSource(ds.getTypeNames()[0]).getFeatures()));
                ds.dispose();
            }
            return CollecMgmt.exportSFC(CollecMgmt.mergeSFC(sfcs, keepAttributes, boundFile), fileOut, ".gpkg", true);
        } else {
            SimpleFeatureCollection sfc = null;
            for (File f : filesToMerge) {
                DataStore ds = getDataStore(f);
                assert ds != null;
                if (sfc == null)
                    sfc = ds.getFeatureSource(ds.getTypeNames()[0]).getFeatures();
                else {
                    DataStore dsAlreadyMerged = CollecMgmt.getDataStore(fileOut);
                    sfc = CollecMgmt.mergeSFC(Arrays.asList(dsAlreadyMerged.getFeatureSource(dsAlreadyMerged.getTypeNames()[0]).getFeatures(), ds.getFeatureSource(ds.getTypeNames()[0]).getFeatures()), keepAttributes, boundFile);
                    dsAlreadyMerged.dispose();
                }
                CollecMgmt.exportSFC(sfc, fileOut, ".gpkg", true);
                ds.dispose();
            }
            return fileOut;
        }
    }
}
