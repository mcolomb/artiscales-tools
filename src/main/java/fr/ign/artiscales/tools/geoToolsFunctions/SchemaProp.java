package fr.ign.artiscales.tools.geoToolsFunctions;

public class SchemaProp {
    /**
     * Class containing methods to deal with Schemas and/or SimpleFeatureBuilders
     */
    private static String epsg = "EPSG:2154";

    public static String getEpsg() {
        return epsg;
    }

    public static void setEpsg(String epsg) {
        SchemaProp.epsg = epsg;
    }
}
